import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AntibtsPage } from './antibts.page';

const routes: Routes = [
  {
    path: '',
    component: AntibtsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AntibtsPageRoutingModule {}

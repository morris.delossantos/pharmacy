import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AntibtsPageRoutingModule } from './antibts-routing.module';

import { AntibtsPage } from './antibts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AntibtsPageRoutingModule
  ],
  declarations: [AntibtsPage]
})
export class AntibtsPageModule {}

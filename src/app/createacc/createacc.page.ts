import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-createacc',
  templateUrl: './createacc.page.html',
  styleUrls: ['./createacc.page.scss'],
})
export class CreateaccPage implements OnInit {

  constructor() { }
  
  public form = {
    email_address: "",
    password: "",
    repeatpassword: "",
    firstname:"",
    middle:"",
    lastname:""

  }

  ngOnInit() {
  }

  print() {
    console.log(this.form)
  }
}
